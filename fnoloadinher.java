import java.io.*;
class Parent{
    public void add(int a, int b){
        System.out.println(a+"+"+b+"="+(a+b));
    }
}
class Child extends Parent{
    public void add(int a, int b, int c){
        System.out.println(a+"+"+b+"+"+c+"="+(a+b+c));
    }
    public static void main(String[] args) {
        Child t= new Child();
        t.add(10,20);
        t.add(10,20,30);
        t.add(4,5);
        t.add(4,5,6);
    }
}
